https://apotris.com/

A block stacking puzzle game for the Gameboy Advance, with focus on delivering responsive gameplay and satisfying visuals. It follows guideline rules, so all spins, kicks and setups should work as expected.

Join the [discord server](https://discord.com/invite/jQnxmXS7tr) for updates, beta releases, or to give me feedback!

You can find builds/releases at https://apotris.com/downloads
